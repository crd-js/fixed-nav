# Fixed Navigation Bar

```CRD.FixedNav``` provides a way to manage a fixed navigation bar in top of the page. Navigation bar can show or hide depending on scrolling events, use an offset top and botton to define an active area where fixed navigation will work or fix and un-fix the navigation on top, bases on the same offsets, or even match a set of conditions (defined as function) to determine if navigation bar must shor or hide.

```CRD.FixedNav``` inherits ```CRD.ScrollSpy``` to hook to ```window.scroll``` events. See ```CRD.ScrollSpy``` documentation for additional events that can be hooked to ```CRD.FixedNav``` by inheritance.

*Note that styles (for ```width```, ```height``` and ```position```) are not provided.*

### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="function.js"></script>
<script type="text/javascript" src="utils.js"></script>
<script type="text/javascript" src="scroll-spy.js"></script>
<script type="text/javascript" src="fixed-nav.js"></script>
```

Some basic styles:

```css
#fixed {
	position: fixed;
	width: 100%;
	padding: 10px 0;
	background-color: black;
}
```

Anywhere in your ```html``` document:

```html
<div id="fixed">
	CRD.FixedNav Navigation!
</div>
```

Then initialize the navigation bar:

```javascript
// Initializes FixedNav
var fixed = new CRD.FixedNav('#fixed');
```

*A packed and merged source (including function.js, utils.js, scroll-spy.js and fixed-nav.js) is available to use.*

### Arguments

```CRD.AllImagesLoaded``` accepts two arguments:

Argument | Default | Description
-------- | ------- | -----------
element : ```String``` or ```Element``` | --- | Element or selector to use as fixed navigation bar.
options : ```Object``` | ```{ initState : CRD.FixedNav.Mode.CLOSE, delay : 0, conditions : [], hideOnScroll : true, offset : null, positions : { closed : function() { return -this.fixed.height(); }, opened : 0 }, classes : { closed : 'crd-fixednav-closed', opened : 'crd-fixednav-opened' } }``` | Options to overwrite default values.

### Settings

Option | Type | Default | Description
------ | ---- | ------- | -----------
initState | ```String``` | ```CRD.FixedNav.Mode.CLOSE``` | State for the navigation bar. Default to closed.
delay | ```Number``` | ```0``` | Delay to use for ```debounce``` (delay to execute events, see ```CRD.Utils``` documentation).
conditions | ```Array``` | ```[]``` | Conditions to check before showing or hidding the navigation bar. Each condition must be a ```function``` and must return a ```boolean``` (```true``` if condition matched, otherwise ```false```).
hideOnScroll | ```Boolean``` | ```true``` | Navigation bar must hide when scrolling down?
offset | ```Object``` | ```null``` | Defined offsets (as ```offset['top']``` and ```offset['bottom']```) to use as margins for an active area (where navigation bar should be active).
positions | ```Object``` | ```{ closed : function() { return -this.fixed.height(); }, opened : 0 }``` | Navigation bar positions (top) for open and close states. Each state can be a ```Number``` representing a position, or a ```function``` that returns a position. By default, navigation bar is fixed to top (```positions.opened : 0```) and uses the navigation bar negative top position for closed state (thru the defined function).
classes | ```Object``` | ```{ closed : 'crd.fixednav-closed', opened : 'crd-fixednav-opened' }``` | Classes to toggle when navigation bar is opened or closed.

### Events

Event | Params | Description
----- | ------ | -----------
```crd.fixednav.active``` | element : ```Element```, object : ```CRD.FixedNav``` | Triggered when the scrolling area is on the "active" zone (defined by conditions and offsets).
```crd.fixednav.inactive``` | element : ```Element```, object : ```CRD.FixedNav``` | Triggered when the scrolling area is outside the "active" zone (defined by conditions and offsets).
```crd.fixednav.open``` | element : ```Element```, object : ```CRD.FixedNav``` | Triggered when the navigation bar is shown.
```crd.fixednav.close``` | element : ```Element```, object : ```CRD.FixedNav``` | Triggered when the navigation bar hides.

### Methods

Method | Arguents | Returns | Description
------ | -------- | ------- | -----------
constructor | element : ```String``` or ```Element```, options : ```Object``` | A reference to the current object or class: ```CRD.FixedNav```. | Initializes the instance for the element with the provided options.
handleScroll | direction : ```String```, scrollY : ```Number``` | Handles the scrolling events, to determine if navigation bar must show or hide.
switchMenu | state : ```String``` | --- | Toggles the menu visibility.
evalConditions | -- | ```Boolean``` | Eval all defined conditions, and returns ```true``` ir all conditions are met.
getOffset | offset : ```String``` | ```Number```or ```Boolean``` | Get the offset (top or bottom) and returns the offset amount. Otherwise returns ```false```.
getValue | value : ```Function``` or ```Number``` | ```Number```or ```Boolean``` | Get the value from a value passed as a ```number``` or ```function```. If a ```function``` is supplied as a value, it must return a ```number``` or a ```boolean```.
destroy | --- | --- | Unhooks from listened events.

### Constants

Property | Type | Description
------ | -------- | -----------
CRD.FixedNav.Mode.OPEN | ```String``` | Indicates the opened value.
CRD.FixedNav.Mode.CLOSE | ```String``` | Indicates the closed value.

### Properties

Property | Type | Description
------ | -------- | -----------
state | ```String``` | State of the fixed navigation bar (```'open'``` or ```'close'```).

### jQuery Helper

A jQuery initialization helper is available to use directly with a jQuery object selector.

#### Example

Initialization thru the jQuery helper:

```javascript
// Initializes FixedNav
var fixed = jQuery('#fixed').fixednav();

// Gets the instance for the same object
fixed === jQuery('#fixed').fixednav('get'); // true

// Close the menu
jQuery('#fixed').fixednav('switchMenu', CRD.FixedNav.Mode.CLOSE);
```

#### Arguments

The jQuery helper accepts several arguments, as described below:

Arguments | Example usage | Description
--------- | ------------- | -----------
options : ```Object``` | ```jQuery('.selector).fixednav({ hideOnScroll : false })``` | Providing a single argument as an ```Object``` will initialize the ```CRD.FixedNad``` using the passed object as initialization options.
getter : ```String``` | ```jQuery('.selector).fixednav('get')``` | Providing the ```'get'``` argument, will get the instance of ```CRD.FixedNav``` for the selected element.
method : ```String```, ... ```*``` | ```jQuery('.selector).fixednav('switchMenu', CRD.Fixednav.Mode.OPEN)``` | Providing a valid method name (as a ```String```) an its arguments, will apply the method to the instance of ```CRD.FixedNav``` stored for the selected element.

### Examples

Initializes a fixed navigation bar (using a custom element form a webpage) that will always be visible, and will become fixed when user scroll at least 500px down.

```javascript
// Initializes the fixed navigation bar
var fixed = new CRD.FixedNav('#fixed', {
	initState    : CRD.FixedNav.Mode.OPEN,
	hideOnScroll : false,
	offset       : {
		top : 500
	},
	on           : {
		'crd.fixednav.active'   : function(el) {
			el.css('position', 'fixed');
		},
		'crd.fixednav.inactive' : function(el) {
			el.css('position', 'static');
		}
	}
});
```

*Note that the object events could be set thru the initialization options, as provided by ```CRD.Utils``` (thru setting object options, see CRD.Utils documentation), events can be setted as options when initializing the object.*

### Dependencies

- CRD.Utils
- CRD.ScrollSpy
- jQuery [http://www.jquery.com](jQuery)

### License

Copyright (c) 2016 Luciano Giordano

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details